from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np


class Paecklipacker(BaseEstimator, TransformerMixin):
    """make 3D brain into small packages, for histograms
    note: size_paecklis is per dimension"""
    def __init__(self, nbr_paecklis, size_paecklis=10,
                 nbr_slizes=100, height=155, width=140):
        self.size_paecklis = size_paecklis
        self.nbr_slizes = nbr_slizes
        self.height = height
        self.width = width
        self.nbr_paecklis = nbr_paecklis

    def fit(self, X, y=None):
        print('Paecklipacker fit called - did nothing')
        return self

    def transform(self, X, y=None):
        # X = check_array(X)
        return X

    def pack_paeckli(self, X_row):
        # pack paeckli
        x_steps = range(0, self.nbr_slizes, self.size_paecklis)
        y_steps = range(0, self.height, self.size_paecklis)
        z_steps = range(0, self.width, self.size_paecklis)

        # get paecklis
        X_cube = np.reshape(X_row, (self.nbr_slizes, self.height, self.width))

        paecklis = np.zeros((self.nbr_paecklis, self.size_paecklis,
                             self.size_paecklis, self.size_paecklis))

        ctr = 0
        for x in x_steps:
            for y in y_steps:
                for z in z_steps:
                    paecklis[ctr] = X_cube[x:x+self.size_paecklis,
                                           y:y+self.size_paecklis,
                                           z:z+self.size_paecklis]
                    ctr = ctr + 1

        return paecklis


class Histogrammer(BaseEstimator, TransformerMixin):
    """Get Histograms"""
    def __init__(self, min_b=1, max_b=1500, nbr_bins=100,
                 size_paecklis=3, save_x=False, nbr_slizes=100,
                 height=155, width=140):

        self.save_x = save_x
        self.min_b = min_b
        self.max_b = max_b
        self.nbr_bins = nbr_bins
        self.size_paecklis = size_paecklis
        self.nbr_slizes = nbr_slizes
        self.height = height
        self.width = width

        self.nbr_paecklis = (self.nbr_slizes//self.size_paecklis) * \
                            (self.height//self.size_paecklis) * \
                            (self.width//self.size_paecklis)

        self.paecklipacker = Paecklipacker(nbr_paecklis=self.nbr_paecklis,
                                           size_paecklis=size_paecklis,
                                           nbr_slizes=nbr_slizes,
                                           height=height, width=width)

    def fit(self, X, y=None):
        # X = check_array(X)

        print('Histogrammer fit called - did nothing', flush=True)
        return self

    def transform(self, X, y=None):
        # check_is_fitted(self, ["components"])
        # X = check_array(X)
        nbr_images = X.shape[0]

        # do histos
        histo = np.zeros((nbr_images, self.nbr_paecklis, self.nbr_bins))

        for i in range(nbr_images):
            X_row = X[i]
            paecklis = self.paecklipacker.pack_paeckli(X_row)

            for j in range(self.nbr_paecklis):
                histo[i, j] = np.histogram(paecklis[j, :],
                                           bins=self.nbr_bins,
                                           range=(self.min_b, self.max_b))[0]

        print('return histos', flush=True)
        return histo.reshape(nbr_images, -1)
