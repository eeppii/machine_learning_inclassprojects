from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_array
import numpy as np


class Cropper(BaseEstimator, TransformerMixin):
    """Crop images
    min_max_slize: which slizes to be taken
    image_siza_array = [heightmin,heightmax,widthmin,widthmax]"""
    def __init__(self, min_slize=50, nbr_slizes=100,
                 min_height=25, height=155, min_width=10, width=140):
        self.min_slize = min_slize
        self.max_slize = min_slize + nbr_slizes
        self.min_height = min_height
        self.max_height = min_height + height
        self.min_width = min_width
        self.max_width = min_width + width

    def fit(self, X, y=None):
        print('Cropper fit called - did nothing')
        return self

    def transform(self, X, y=None):
        X = check_array(X)

        # prepare shape to 4D
        X = np.reshape(X, (-1, 176, 208, 176))
        all_cubes_shape = X.shape
        nbr_cubes = all_cubes_shape[0]

        # crop
        X_new = X[:, self.min_slize:self.max_slize,
                  self.min_height:self.max_height,
                  self.min_width:self.max_width]

        # reshape
        X_new = np.reshape(X_new, (nbr_cubes, -1))

        print('Cropper transform called - cropped images, is now: ',
              X_new.shape, flush=True)
        # return X_gauss_subsampled
        return X_new
