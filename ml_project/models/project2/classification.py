import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_array, check_is_fitted
from sklearn.ensemble import RandomForestClassifier
from scipy.stats import spearmanr


class RandomForestWrapper(BaseEstimator, TransformerMixin):
    """wrapper for random forest classifier"""
    def __init__(self, n_estimators=1000, max_features="auto",
                 bootstrap=True, n_jobs=-1, class_weight="balanced",
                 b_push=1, c_push=1, d_push=1):

        self.classifier = None
        self.n_estimators = n_estimators
        self.max_features = max_features
        # at bootstrap maybe try "balanced_subsample" or give manually...
        # weights according to distribution in given training data
        self.bootstrap = bootstrap
        self.n_jobs = n_jobs
        self.class_weight = class_weight
        self.b_push = b_push
        self.c_push = c_push
        self.d_push = d_push

    def fit(self, X, y):
        print('classify.fit called with shape: ', X.shape, 'y_shape:', y.shape)
        # initialize classifier
        self.classifier = RandomForestClassifier(
           n_estimators=self.n_estimators, max_features=self.max_features,
           bootstrap=self.bootstrap, n_jobs=self.n_jobs,
           class_weight=self.class_weight)
        # modify y and maybe X make it compatible with classifier
        # modify X -> every features 4 times repeated
        # modify y -> every class on it's own row, and make
        # sample_weight according to y-values
        # sample weight according to probability of y_class
        X_shape = X.shape
        X_modified = np.zeros([4*X_shape[0], X_shape[1]])
        y_modified = [None]*(4*X_shape[0])
        sample_weight = np.zeros([4*X_shape[0]])

        # push y
        y[:, 1] = y[:, 1]*self.b_push
        y[:, 2] = y[:, 2]*self.c_push
        y[:, 3] = y[:, 3]*self.d_push

        for i in range(X_shape[0]):
            # repeat X_sample for each class
            X_modified[i*4:(i+1)*4, :] = np.tile(X[i, :], [4, 1])
            # set y classes
            # maybe use here int 1 2 3 4 for classes?????
            y_modified[i*4:(i+1)*4] = ['a', 'b', 'c', 'd']
            # set sample weight according to prob of above classes
            sample_weight[i*4:(i+1)*4] = y[i, :]

        # call fit of classifier
        self.classifier.fit(X_modified, y_modified,
                            sample_weight=sample_weight)
        return self

    def predict_proba(self, X):
        y_predict = self.classifier.predict_proba(X)

        # get used classes_
        # classes_ = self.classifier.classes_

        print('predict_proba called')
        # print('classes_: ',classes_)
        # print('y_predict: ',y_predict)

        # modify y_predict to match output format
        # edit: already does match!

        return y_predict

    def score(self, X, y):
        print('score called')
        print('X.shape: ', X.shape, '\n y.shape: ', y.shape)
        y_predict = self.predict_proba(X)

        # get simple error for testing
        # diff = np.sqrt((y_predict - y)**2)
        # return -np.mean(diff)

        # NOt recommended: use default, attention: uses only self.predict(X)
        # return self.classifier.score(X,y)

        # use spearman rank correlation (with avg)
        nbr_pred = y_predict.shape[0]
        sum_rho = 0
        for i in range(nbr_pred):
            rho, _ = spearmanr(y_predict[i, :], y[i, :])
            sum_rho = sum_rho + rho
        return sum_rho/nbr_pred


class MeanPredictor(BaseEstimator, TransformerMixin):
    """docstring for MeanPredictor"""
    def fit(self, X, y):
        self.mean = y.mean(axis=0)
        return self

    def predict_proba(self, X):
        check_array(X)
        check_is_fitted(self, ["mean"])
        n_samples, _ = X.shape
        return np.tile(self.mean, (n_samples, 1))
