from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_array
import numpy as np
from sklearn.preprocessing import Normalizer


class hippocampusSlctr(BaseEstimator, TransformerMixin):
    """Selects the Hippocampus on both sides
    uses dim: [left:minwidth,width,minlength,length,minheight,height]"""
    def __init__(self):
        pass

    def slct_hippocampus(self, X, dim_left, dim_right, dim_mid):
        X = check_array(X)
        self.dim_left = dim_left
        self.dim_right = dim_right
        self.dim_mid = dim_mid

        # prepare shape to 4D
        X = np.reshape(X, (-1, 176, 208, 176))
        all_cubes_shape = X.shape
        nbr_cubes = all_cubes_shape[0]

        # crop left
        m_width, width, min_length, length, min_height, height = self.dim_left
        hippo_left = X[:, m_width: m_width + width,
                       min_length: min_length + length,
                       min_height: min_height + height]

        # crop right
        m_width, width, min_length, length, min_height, height = self.dim_right
        hippo_right = X[:, m_width: m_width + width,
                        min_length: min_length + length,
                        min_height: min_height + height]

        # dim mid
        m_width, width, min_length, length, min_height, height = self.dim_mid
        mid = X[:, m_width:m_width + width,
                min_length:min_length + length,
                min_height:min_height + height]

        # reshape
        hippo_right = np.reshape(hippo_right, (nbr_cubes, -1))
        hippo_left = np.reshape(hippo_left, (nbr_cubes, -1))
        mid = np.reshape(mid, (nbr_cubes, -1))
        print('shape_left,r:', hippo_left.shape, hippo_right.shape)
        print('dims lr: ', self.dim_left, self.dim_right)

        #############
        # #visualize
        # import matplotlib.pyplot as plt
        # # a = plt.hist(hippo_right.reshape(-1,1),bins=36,range=(1,4418))
        # b = plt.hist(hippo_left[8,:].reshape(-1,1),bins=36,range=(2000,4418))
        # plt.show()
        # print('showing')
        ###############

        print('slct_hippocampus called')
        return hippo_left, hippo_right, mid


class Histogrammer(BaseEstimator, TransformerMixin):
    """Get Histograms with some part of paecklipacker"""
    def __init__(self, min_b=1, max_b=1500, nbr_bins=100,
                 dim_left=[47, 29, 95, 24, 43, 28],
                 dim_right=[100, 26, 95, 24, 45, 23],
                 dim_mid=[50, 75, 70, 90, 85, 10],
                 nbr_histo_pl=3):

        self.min_b = min_b
        self.max_b = max_b
        self.nbr_bins = nbr_bins
        self.dim_left = dim_left
        self.dim_right = dim_right
        self.dim_mid = dim_mid

        self.nbr_histo_pl = nbr_histo_pl
        self.nbr_histo_tot = nbr_histo_pl**3

        self.hippocampusslctr = hippocampusSlctr()

        self.normalizer = Normalizer()

    def fit(self, X, y=None):
        # X = check_array(X)
        return self

    def transform(self, X, y=None):
        hippo_left, hippo_right, mid = self.hippocampusslctr.slct_hippocampus(
                                       X, self.dim_left,
                                       self.dim_right, self.dim_mid)
        hippo_list = [hippo_left, hippo_right, mid]
        dim_list = [self.dim_left, self.dim_right, self.dim_mid]

        nbr_images = X.shape[0]

        # do histos
        histo = np.zeros((nbr_images,
                          len(hippo_list)*self.nbr_histo_tot,
                          self.nbr_bins))
        for k in range(len(hippo_list)):
            hippo = hippo_list[k]
            dim = dim_list[k]
            xd, yd, zd = dim[1], dim[3], dim[5]
            # get steps
            x_steps = list(range(0, xd, int(np.round(
                      xd/self.nbr_histo_pl))))[0:3]
            x_steps.append(xd)
            y_steps = list(range(0, yd, int(np.round(
                      yd/self.nbr_histo_pl))))[0:3]
            y_steps.append(yd)
            z_steps = list(range(0, zd, int(np.round(
                      zd/self.nbr_histo_pl))))[0:3]
            z_steps.append(zd)

            for i in range(nbr_images):
                # get one image
                hippo_row = hippo[i, :]
                hippo_cube = np.reshape(hippo_row, (xd, yd, zd))
                # loop through paecklis
                ctr = 0
                for a in range(len(x_steps)-1):
                    for b in range(len(y_steps)-1):
                        for c in range(len(z_steps)-1):
                            paeckli = hippo_cube[x_steps[a]:x_steps[a+1],
                                                 y_steps[b]:y_steps[b+1],
                                                 z_steps[c]:z_steps[c+1]]

                            # calc histo
                            histo_curr = np.histogram(paeckli,
                                                      bins=self.nbr_bins,
                                                      range=(self.min_b,
                                                             self.max_b))[0]
                            histo[i,
                                  (k*self.nbr_histo_tot)
                                  + ctr] = self.normalizer.transform(
                                           np.float64(
                                               histo_curr.reshape(1, -1)))
                            ctr = ctr+1

        print('featselect.histogrammer transform, ret histos', flush=True)
        a = histo.reshape(nbr_images, -1)
        print(a[1, :])
        return a
