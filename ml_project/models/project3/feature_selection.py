from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
from sklearn.preprocessing import Normalizer
import math
from scipy.signal import butter, lfilter, welch, find_peaks_cwt
from scipy.stats import skew, kurtosis
from sklearn.feature_selection import VarianceThreshold


class Cropper(BaseEstimator, TransformerMixin):
    """Selects the interesting FFT coeffs"""
    def __init__(self):
        pass

    def crop_data(self, X):
        "delete first unuseful datapoints, delte all zeros"

        # delete first 500 points
        X_cropped = X[:, 500:]
        x_shape = X_cropped.shape

        # delte zeros at end
        X_useful = [None]*x_shape[0]

        for i in range(x_shape[0]):
            x_curr = X_cropped[i]
            x_nonzero = self.delte_zeros(x_curr)
            X_useful[i] = x_nonzero

        print('cropper called', flush=True)
        return X_useful

    def delte_zeros(self, x):
        "returns an array without last zeros, except all are zeros"
        ll = len(x)

        for idx in range(1, ll+1):
            if x[- idx] != 0.0:
                return x[0:ll - idx + 1]

        return x


class PreFilter(BaseEstimator, TransformerMixin):
    """Prefilter X, make ready for FFT, cut off freq
    below heart-beat, above 40Hz too (paper)"""
    def __init__(self):
        pass

    def butter_bandpass(self, lowcut, highcut, fs, order):
        "fs is sampling freq in Hz, cutoff in Hz too"
        nyq = 0.5*fs
        # normalized to nyq from 0 to 1
        normal_lowcut = lowcut / nyq
        normal_highcut = highcut / nyq
        b, a = butter(order, [normal_lowcut,
                      normal_highcut], btype='band')
        return b, a

    def butter_bandpass_filter(self, data, lowcut, highcut, fs, order):
        b, a = self.butter_bandpass(lowcut, highcut, fs, order)
        y = lfilter(b, a, data)
        return y

    def apply_filter(self, X, lowcut, highcut, fs, order):

        ll = len(X)

        for i in range(ll):
            X[i] = self.butter_bandpass_filter(
                 X[i],
                 lowcut, highcut, fs, order)

        print('pre_filter called', flush=True)
        return X


class WelchWrapper(BaseEstimator, TransformerMixin):
    """applies scipy.signal.welch"""
    def __init__(self):
        pass

    def apply_welch(self, X, window, nperseg, noverlap, return_onesided):
        # call every row of X, apply welch
        ll = len(X)
        nbr_keep = math.ceil((nperseg-1)/2)+1
        X_welch = np.zeros([ll, nbr_keep])

        for i in range(ll):
            x_curr = X[i]

            _, X_welch[i, :] = welch(
                    x_curr, window=window, nperseg=nperseg,
                    noverlap=noverlap, return_onesided=return_onesided)
            X_welch[i, 0] = np.linalg.norm(X_welch[i, 0])

        print('welch was called, X_hape now: ', X_welch.shape)
        return X_welch


class FFTApplier(BaseEstimator, TransformerMixin):
    """Apply FFT to data"""
    def __init__(self, lowcut=0.5, highcut=40, fs=330, order=5,
                 window='hanning', nperseg=256, noverlap=None,
                 return_onesided=True, use_TD_features=True,
                 use_peaks=True, nbr_bins=80, mag_max=2000,
                 beat_max=1000, peak_threshold=1.1, name='train',
                 bin_factor=1):

        self.lowcut = lowcut
        self.highcut = highcut
        self.fs = fs
        self.order = order

        self.window = window
        self.nperseg = nperseg
        if noverlap == 'None':
            noverlap = None
        self.noverlap = noverlap
        self.return_onesided = return_onesided

        self.normalizer = Normalizer()
        # self.coeffslctr = CoeffSlctr()
        self.prefilter = PreFilter()
        self.cropper = Cropper()
        self.welch_wrapper = WelchWrapper()

        self.use_TD_features = use_TD_features
        self.calc_td_features = CalcTDFeatures()

        self.use_peaks = use_peaks
        self.nbr_bins = nbr_bins
        self.mag_max = mag_max
        self.beat_max = beat_max
        self.peak_threshold = peak_threshold
        self.calc_peaks_features = CalcPeakFeatures()

        self.name = name

        self.variance_threshold = VarianceThreshold()

        self.bin_factor = bin_factor

    def fit(self, X, y=None):
        # X = check_array(X)
        return self

    def transform(self, X, y=None):

        X_shape = X.shape

        # take apart the original X and X_peaks from preprocessing
        if X_shape[1] > 18286:
            print('took apart the X')
            X_original = X[:, 0:18286]
            X_matrix = X[:, 18286:]
        else:
            print('got original size X')
            X_original = X

        # crop data, X_useful is list!
        X_useful = self.cropper.crop_data(X_original)

        # bandpass filter, X_filt is list!
        X_filt = self.prefilter.apply_filter(
                    X_useful, self.lowcut,
                    self.highcut, self.fs, self.order)

        # get fft coeff by welch method, X_welch is matrix!
        X_welch = self.welch_wrapper.apply_welch(
                         X_filt, window=self.window,
                         nperseg=self.nperseg,
                         noverlap=self.noverlap,
                         return_onesided=self.return_onesided)

        X_new = self.normalizer.transform(X_welch)

        if self.use_TD_features:  # X_TD is matrix
                X_TD = self.calc_td_features.calc_features(X_filt)
                X_new = np.concatenate((X_TD, X_new), axis=1)

        if self.use_peaks:  # X_peaks is matrix
            X_peaks = self.calc_peaks_features.calc_features(
                X_filt, X_matrix, nbr_bins=self.nbr_bins,
                mag_max=self.mag_max, beat_max=self.beat_max,
                peak_threshold=self.peak_threshold,
                bin_factor=self.bin_factor)
            X_new = np.concatenate((X_peaks, X_new), axis=1)

        print('FFTApplier transform called', flush=True)
        return X_new


class CalcTDFeatures(BaseEstimator, TransformerMixin):
    """applies scipy.signal.welch"""
    def __init__(self):
        self.normalizer = Normalizer()
        pass

    def calc_features(self, X):
        # call every row of X, apply welch
        ll = len(X)
        nbr_features = 8
        X_features = np.zeros([ll, nbr_features])

        for i in range(ll):
            x_curr = X[i]
            nbr_pts = len(x_curr)

            energy = np.sum(x_curr**2)/nbr_pts
            mean = np.mean(x_curr)
            stdev = np.std(x_curr)
            maxima = max(x_curr)
            minima = min(x_curr)
            skewness = skew(x_curr)
            kurtness = kurtosis(x_curr)
            diff = maxima - minima

            X_features[i, :] = [
                    energy, mean,
                    stdev, maxima, minima,
                    skewness, kurtness, diff]

        X_features = self.normalizer.transform(X_features)
        print('calc_features was called')
        return X_features


class CalcPeakFeatures(BaseEstimator, TransformerMixin):
    """applies scipy.signal.welch"""
    def __init__(self):
        self.normalizer = Normalizer()
        pass

    def calc_features(self, X_filt, X_matrix,
                      nbr_bins, mag_max, beat_max,
                      peak_threshold, bin_factor):
        # start own process for every sample, apply peakfinder to each of those
        print('peakfinder called, unpack matrix and do peaks')

        ll = len(X_filt)
        peak_matrix = np.zeros([ll, nbr_bins + int(nbr_bins * bin_factor)])

        # matrix to list(list)
        peak_list = self.matrix_to_list(X_matrix)

        for i in range(ll):
            histo_mag, histo_beat = self.peak_process(
                X_filt[i],
                peak_list[i], nbr_bins, mag_max, beat_max,
                peak_threshold, bin_factor)
            # in peak_list the peak histos are stored
            peak_matrix[i, :nbr_bins] = histo_mag
            peak_matrix[i, nbr_bins:] = histo_beat

        peak_matrix = self.normalizer.transform(peak_matrix)

        print('peakfinder terminated propperly')
        return peak_matrix

    def peak_process(self, X_row, peak_idx,
                     nbr_bins, mag_max, beat_max,
                     peak_threshold, bin_factor):

            # get only peaks above a certain threshold!
            peak_idx = peak_idx.astype(int)
            peak_mean = np.mean(X_row[peak_idx])
            good_peaks = np.where(
                X_row[peak_idx] > (peak_threshold * peak_mean))
            peak_idx = peak_idx[good_peaks]

            histo_mag = np.histogram(
                        X_row[peak_idx],
                        bins=nbr_bins, range=(-mag_max, mag_max))[0]
            beat_lengths = peak_idx[1:]-peak_idx[:-1]
            histo_beat = np.histogram(
                        beat_lengths, bins=int(nbr_bins*bin_factor),
                        range=(0, beat_max))[0]

            return histo_mag, histo_beat

    def matrix_to_list(self, X):
        "delete first unuseful datapoints, delte all zeros"

        x_shape = X.shape

        # delte zeros at end
        X_useful = [None] * x_shape[0]

        for i in range(x_shape[0]):
            x_curr = X[i, :]
            x_nonzero = self.delte_zeros(x_curr)
            X_useful[i] = x_nonzero

        print('cropper called', flush=True)
        return X_useful

    def delte_zeros(self, x):
        "returns an array without last zeros, except all are zeros"
        ll = len(x)

        for idx in range(1, ll+1):
            if x[-idx] != 0.0:
                return x[0:ll-idx+1]

        return x


# preprocessing

class Preprocessor(BaseEstimator, TransformerMixin):
    """preprocessing"""
    def __init__(self, lowcut=0.5, highcut=40, fs=300,
                 order=5, peak_window=[5, 30, 1], name='train'):

        self.lowcut = lowcut
        self.highcut = highcut
        self.fs = fs
        self.order = order

        self.normalizer = Normalizer()
        self.prefilter = PreFilter()
        self.cropper = Cropper()

        self.peak_window = peak_window
        self.name = name

    def fit(self, X, y=None):
        # X = check_array(X)
        return self

    def transform(self, X, y=None):

        print('start preprocesor')

        X_shape = X.shape

        # crop data, X_useful is list!
        X_useful = self.cropper.crop_data(X)

        # bandpass filter, X_filt is list!
        X_filt = self.prefilter.apply_filter(
                X_useful, self.lowcut, self.highcut,
                self.fs, self.order)

        # create raw peaks, X_peaks is list!
        X_peaks = self.create_peaks(
                X_filt, peak_window=self.peak_window)

        # make matrix
        X_matrix = self.make_matrix(X_peaks)
        print(X_matrix[0, 0:20])

        if X_shape[0] == 6822:
            X_matrix = np.concatenate((X, X_matrix), axis=1)
            np.save(
                '/cluster/home/thomasep/ML/\
                ml-project/data/short_data/train_data_short', X_matrix)
        else:
            X_matrix = np.concatenate((X, X_matrix), axis=1)
            np.save(
                '/cluster/home/thomasep/\
                ML/ml-project/data/short_data/test_data_short', X_matrix)

        print('preprocessor done', flush=True)
        return X_matrix

    def create_peaks(self, X_filt, peak_window):

        ll = len(X_filt)
        X_peaks = [None]*ll

        for i in range(ll):
            X_row = X_filt[i]

            # set curve in middle
            # X_row = X_row - np.mean(X_row)
            peak_idx = find_peaks_cwt(
                            X_row, range(
                                peak_window[0],
                                peak_window[1], peak_window[2]))

            X_peaks[i] = peak_idx

        return X_peaks

    def make_matrix(self, X_peaks):

        # get max size
        ll = len(X_peaks)
        max_l = 0
        for i in range(ll):
            max_n = len(X_peaks[i])
            if max_n > max_l:
                max_l = max_n

        # fill matrix
        matrix = np.zeros([ll, max_l])
        for i in range(ll):
            max_n = len(X_peaks[i])
            matrix[i, 0:max_n] = X_peaks[i]

        return matrix
