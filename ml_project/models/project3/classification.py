import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import f1_score
from sklearn.ensemble import GradientBoostingClassifier


class SVMWrapper(BaseEstimator, TransformerMixin):
    """wrapper for random forest classifier"""
    def __init__(self, C=1.0, kernel='rbf', degree=3,
                 gamma='auto',
                 coef0=0.0, probability=False, shrinking=True,
                 tol=0.001,
                 class_weight='balanced', max_iter=-1,
                 decision_function_shape='ovr',
                 random_state=None):

        self.classifier = None

        self.C = C
        self.kernel = kernel
        self.degree = degree
        self.gamma = gamma
        self.coef0 = coef0
        self.probability = probability
        self.shrinking = shrinking
        self.tol = tol
        self.class_weight = class_weight
        self.max_iter = max_iter
        self.decision_function_shape = decision_function_shape
        self.random_state = random_state

    def fit(self, X, y):
        print('initialize SVMWrapper')
        # initialize classifier
        self.classifier = SVC(
              C=self.C, kernel=self.kernel, degree=self.degree,
              gamma=self.gamma, coef0=self.coef0,
              probability=self.probability,
              shrinking=self.shrinking,
              tol=self.tol, class_weight=self.class_weight,
              max_iter=self.max_iter,
              decision_function_shape=self.decision_function_shape,
              random_state=self.random_state)

        # call fit of classifier
        self.classifier.fit(X, y)
        print('classify.fit called with shape: ', X.shape, 'y_shape:', y.shape)
        return self

    def predict(self, X):
        y_predict = self.classifier.predict(X)

        # modify y_predict to match output formats
        y_predict = y_predict.astype(int)

        # make some prints
        print('predict called')
        print('nbr of y_predict :', y_predict.shape[0])
        print('sum of all y_predict: ', np.sum(y_predict))
        print('y_predict line 10 to 30 : ', y_predict[10:30])

        return y_predict

    def score(self, X, y):
        y_predict = self.predict(X)

        test = y_predict[10:50]
        idx_0 = np.where(test == 0)[0]
        idx_1 = np.where(test == 1)[0]
        # idx_2 = np.where(test==2)[0]
        # idx_3 = np.where(test==3)[0]
        if len(idx_0) == 0 or len(idx_1) == 0:
            return 0

        print(len(idx_0), len(idx_1))
        # calc score
        score = f1_score(y, y_predict, average='micro')

        print('score called: ', score)
        return score


class RandomForestWrapper(BaseEstimator, TransformerMixin):
    """wrapper for random forest classifier"""
    def __init__(self, n_estimators=1000, max_features="auto",
                 bootstrap=False, oob_score=False, n_jobs=-1,
                 class_weight="balanced"):

        self.classifier = None
        self.n_estimators = n_estimators
        self.max_features = max_features
        # at bootstrap maybe try "balanced_subsample" or give manually...
        # weights according to distribution in given training data
        self.bootstrap = bootstrap
        self.oob_score = oob_score
        self.n_jobs = n_jobs
        self.class_weight = class_weight

    def fit(self, X, y):
        # initialize classifier
        self.classifier = RandomForestClassifier(
           n_estimators=self.n_estimators, max_features=self.max_features,
           bootstrap=self.bootstrap, oob_score=self.oob_score,
           n_jobs=self.n_jobs,
           class_weight=self.class_weight)

        # call fit of classifier
        self.classifier.fit(X, y)
        print('classify.fit called with shape: ', X.shape, 'y_shape:', y.shape)
        return self

    def predict(self, X):
        y_predict = self.classifier.predict(X)

        # modify y_predict to match output formats
        y_predict = y_predict.astype(int)

        # make some prints
        print('predict called')
        print('nbr of y_predict :', y_predict.shape[0])
        print('sum of all y_predict: ', np.sum(y_predict))
        print('y_predict line 10 to 30 : ', y_predict[10:30])

        return y_predict

    def score(self, X, y):
        y_predict = self.predict(X)

        test = y_predict[10:50]
        idx_0 = np.where(test == 0)[0]
        idx_1 = np.where(test == 1)[0]
        # idx_2 = np.where(test==2)[0]
        # idx_3 = np.where(test==3)[0]
        if len(idx_0) == 0 or len(idx_1) == 0:
            return 0

        print(len(idx_0), len(idx_1))
        # calc score
        score = f1_score(y, y_predict, average='micro')

        print('score called: ', score)
        return score


class GradientBoostingWrapper(BaseEstimator, TransformerMixin):
    """wrapper for random forest classifier"""
    def __init__(self, loss='exponential',
                 learning_rate=0.1, n_estimators=100,
                 subsample=1.0, max_depth=8, min_samples_split=2,
                 min_samples_leaf=1):

        self.classifier = None
        self.loss = loss
        self.learning_rate = learning_rate
        self.n_estimators = n_estimators
        self.subsample = subsample
        self.max_depth = max_depth
        self.min_samples_split = min_samples_split
        self.min_samples_leaf = min_samples_leaf

    def fit(self, X, y):
        print('fit GradientBoostingClassifier called')
        # initialize classifier
        self.classifier = GradientBoostingClassifier(
           loss=self.loss, learning_rate=self.learning_rate,
           n_estimators=self.n_estimators,
           subsample=self.subsample, max_depth=self.max_depth,
           min_samples_leaf=self.min_samples_leaf,
           min_samples_split=self.min_samples_split)

        # call fit of classifier
        self.classifier.fit(X, y)
        print('classify.fit called with shape: ',
              X.shape, 'y_shape:', y.shape)
        return self

    def predict(self, X):
        y_predict = self.classifier.predict(X)

        # modify y_predict to match output formats
        y_predict = y_predict.astype(int)

        # make some prints
        print('predict called')
        print('nbr of y_predict :', y_predict.shape[0])
        print('sum of all y_predict: ', np.sum(y_predict))
        print('y_predict line 10 to 30 : ', y_predict[10:30])

        return y_predict

    def score(self, X, y):
        y_predict = self.predict(X)

        test = y_predict[10:50]
        idx_0 = np.where(test == 0)[0]
        idx_1 = np.where(test == 1)[0]
        # idx_2 = np.where(test==2)[0]
        # idx_3 = np.where(test==3)[0]
        if len(idx_0) == 0 or len(idx_1) == 0:
            return 0

        print(len(idx_0), len(idx_1))
        # calc score
        score = f1_score(y, y_predict, average='micro')

        print('score called: ', score)
        return score
